package dhbw.se.mbfl.refactoring;

/**
 *
 * @author Florian Loch & Maurice Busch
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        test();
    }
    
    public static void test() {
        Customer cust1 = new Customer("Maurice");
        Customer cust2 = new Customer("Florian");
        Movie starwars = new Movie("Starwars", Movie.REGULAR);
        Movie lor = new Movie("Lord of the Rings", Movie.REGULAR);
        Movie dschungelbuch = new Movie("Dschungelbuch", Movie.CHILDRENS);
        Movie godzilla = new Movie("Godzilla (2014)", Movie.NEW_RELEASE);
        
        cust1.addRental(new Rental(lor, 2));
        cust1.addRental(new Rental(starwars, 5));
        
        cust2.addRental(new Rental(dschungelbuch, 1));
        cust2.addRental(new Rental(godzilla, 2));
        
        
        String cust1Expected = "Rental Record for Maurice\n" +
                            "	Lord of the Rings	2.0\n" +
                            "	Starwars	6.5\n" +
                            "Amount owed is 8.5\n" +
                            "You earned 2 frequent renter points";
        
        String cust2Expected = "Rental Record for Florian\n" +
                            "	Dschungelbuch	1.5\n" +
                            "	Godzilla (2014)	6.0\n" +
                            "Amount owed is 7.5\n" +
                            "You earned 3 frequent renter points";
        
        compare(cust1.statement(), cust1Expected);
        compare(cust2.statement(), cust2Expected);
    }
    
    private static void compare(Object obj_1, Object obj_2) {
        if (obj_1.equals(obj_2)) {
            System.out.println("Assertion successful!");
        }
        else {
            System.out.println("Assertion failed!");
            System.out.println("Expected");
            System.out.println(obj_1);
            System.out.println("to be");
            System.out.println(obj_2);
        }
    }
}
